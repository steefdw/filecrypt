<?php

declare(strict_types=1);

namespace Steefdw\FileCrypt\Exceptions;

use Exception;

class InvalidInputFile extends Exception
{
}
