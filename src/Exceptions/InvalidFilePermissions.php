<?php

namespace Steefdw\FileCrypt\Exceptions;

use Exception;

class InvalidFilePermissions extends Exception
{
}
