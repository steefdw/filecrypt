<?php

declare(strict_types=1);

namespace Steefdw\FileCrypt;

use const SODIUM_CRYPTO_SECRETSTREAM_XCHACHA20POLY1305_ABYTES as ADDITIONAL_BYTES;
use function sodium_crypto_secretstream_xchacha20poly1305_init_pull as stream_init_pull;
use function sodium_crypto_secretstream_xchacha20poly1305_init_push as stream_init_push;
use function sodium_crypto_secretstream_xchacha20poly1305_pull as stream_pull;
use function sodium_crypto_secretstream_xchacha20poly1305_push as stream_push;
use const SODIUM_CRYPTO_SECRETSTREAM_XCHACHA20POLY1305_TAG_FINAL as TAG_FINAL;
use SodiumException;
use Steefdw\FileCrypt\Exceptions\InvalidFilePermissions;
use Steefdw\FileCrypt\Exceptions\InvalidInputFile;
use Steefdw\FileCrypt\Exceptions\InvalidInvocation;
use Steefdw\FileCrypt\Exceptions\InvalidKey;
use Steefdw\FileCrypt\FileSystem\ReadFile;
use Steefdw\FileCrypt\FileSystem\WriteFile;

final class FileCrypt
{
    private const CHUNK_SIZE = 4096;

    /**
     * Don't allow this class to be instantiated.
     *
     * @throws InvalidInvocation
     */
    private function __construct()
    {
        throw new InvalidInvocation(__CLASS__ . ' can\'t be instantiated');
    }

    /**
     * Secret key required to encrypt/decrypt the stream.
     * Note: this is a binary sting, so expect something like:
     * - '���m���yS'������H�&�@�'4%�\F�3'
     * And not a key like:
     * - 'abcdefg1234567890hijkjlmnopk'.
     *
     * If you want to put this in an env variable, save it as:
     * `base64_encode(FileCrypt::generateKey());`.
     *
     * The $secretKey that you will use is:
     * `base64_decode(config('filecrypt.my-secret-key'));`.
     */
    public static function generateKey(): string
    {
        return sodium_crypto_secretstream_xchacha20poly1305_keygen();
    }

    public static function isValidKey(string $key): bool
    {
        return strlen($key) === SODIUM_CRYPTO_STREAM_KEYBYTES;
    }

    /**
     * Simple encryption of files with Libsodium.
     *
     * @see https://doc.libsodium.org/secret-key_cryptography/secretstream
     * @see https://github.com/jedisct1/libsodium-php#encrypt-a-file-using-a-secret-key
     * @see https://www.php.net/manual/en/function.sodium-crypto-secretstream-xchacha20poly1305-push.php
     *
     * @throws Exceptions\InvalidFilePermissions
     * @throws InvalidKey|SodiumException
     */
    public static function encrypt(string $originalFilePath, string $encryptedFilePath, string $secretKey): void
    {
        $originalFile = new ReadFile($originalFilePath);
        $encryptedFile = new WriteFile($encryptedFilePath);

        self::writeStream($originalFile, $encryptedFile, $secretKey);

        $encryptedFile->close();
        $originalFile->close();
    }

    /**
     * Simple decryption of files with Libsodium.
     *
     * @see https://www.php.net/manual/en/function.sodium-crypto-secretstream-xchacha20poly1305-pull.php
     *
     * @throws InvalidFilePermissions|InvalidInputFile|InvalidKey|SodiumException
     */
    public static function decrypt(string $encryptedFilePath, string $decryptedFilePath, string $secretKey): void
    {
        $encryptedFile = new ReadFile($encryptedFilePath);
        $decryptedFile = new WriteFile($decryptedFilePath);

        self::readStream($encryptedFile, $decryptedFile, $secretKey);
        $isEndOfFile = $encryptedFile->isEof();

        $decryptedFile->close();
        $encryptedFile->close();

        if (!$isEndOfFile) {
            throw new InvalidInputFile('Invalid or corrupted input');
        }
    }

    /**
     * @throws InvalidFilePermissions
     * @throws SodiumException|InvalidKey
     */
    private static function writeStream(ReadFile $source, WriteFile $target, string $secretKey): void
    {
        if (!self::isValidKey($secretKey)) {
            throw new InvalidKey();
        }

        // Set up a new stream: initialize the state and create the header.
        [$stream, $header] = stream_init_push($secretKey);

        $target->writeChunk($header);
        do {
            $chunk = $source->readChunk(self::CHUNK_SIZE);
            $tag = $source->getTag();

            $encryptedChunk = stream_push($stream, $chunk, '', $tag);
            $target->writeChunk($encryptedChunk);
        } while ($tag !== TAG_FINAL);
    }

    /**
     * @throws InvalidFilePermissions
     * @throws SodiumException|InvalidKey
     */
    private static function readStream(ReadFile $source, WriteFile $target, string $secretKey): void
    {
        if (!self::isValidKey($secretKey)) {
            throw new InvalidKey();
        }

        $stream = stream_init_pull($source->getHeader(), $secretKey);
        do {
            $chunk = $source->readChunk(self::CHUNK_SIZE + ADDITIONAL_BYTES);
            [$decryptedChunk, $tag] = (array) stream_pull($stream, $chunk);

            $target->writeChunk($decryptedChunk);
        } while (!$source->isEof() && $tag !== TAG_FINAL);
    }
}
