<?php

declare(strict_types=1);

namespace Steefdw\FileCrypt\FileSystem;

use Steefdw\FileCrypt\Exceptions\InvalidFilePermissions;

class WriteFile extends AbstractFile
{
    public function __construct(string $filePath)
    {
        if (!file_exists($filePath)) {
            touch($filePath);
        }
        if (!is_writeable($filePath)) {
            throw new InvalidFilePermissions('Could not open file path for writing');
        }

        $filePointer = fopen($filePath, 'wb');
        if (!is_resource($filePointer)) {
            $this->filePointer = null;
            throw new InvalidFilePermissions('Could not open file resource for writing');
        }
        $this->filePointer = $filePointer;
    }

    public function writeChunk(string $data): void
    {
        fwrite($this->filePointer, $data);
    }
}
