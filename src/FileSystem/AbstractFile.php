<?php

namespace Steefdw\FileCrypt\FileSystem;

class AbstractFile
{
    /** @var null|resource */
    protected $filePointer;

    /**
     * Always close the file pointer.
     */
    public function __destruct()
    {
        $this->close();
    }

    public function close(): void
    {
        if ($this->filePointer) {
            fclose($this->filePointer);
            $this->filePointer = null;
        }
    }
}
