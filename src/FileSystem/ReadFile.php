<?php

declare(strict_types=1);

namespace Steefdw\FileCrypt\FileSystem;

use Steefdw\FileCrypt\Exceptions\InvalidFilePermissions;

class ReadFile extends AbstractFile
{
    public function __construct(string $filePath)
    {
        if (!is_readable($filePath)) {
            throw new InvalidFilePermissions('Could not open file path for reading');
        }

        $filePointer = fopen($filePath, 'rb');
        if (!is_resource($filePointer)) {
            $this->filePointer = null;
            throw new InvalidFilePermissions('Could not open file resource for reading');
        }
        $this->filePointer = $filePointer;
    }

    /**
     * @throws InvalidFilePermissions
     */
    public function getHeader(): string
    {
        fseek($this->filePointer, 0, SEEK_SET);

        return $this->readChunk(SODIUM_CRYPTO_SECRETSTREAM_XCHACHA20POLY1305_HEADERBYTES);
    }

    /**
     * @param int<0, max> $length
     *
     * @return string
     *
     * @throws InvalidFilePermissions
     */
    public function readChunk(int $length): string
    {
        $chunk = fread($this->filePointer, $length);
        if (!is_string($chunk)) {
            throw new InvalidFilePermissions('Could not open file for reading');
        }

        return $chunk;
    }

    public function isEof(): bool
    {
        return feof($this->filePointer);
    }

    /**
     * When the file pointer is at the end-of-file, set the tag to `TAG_FINAL`
     * to indicate that this is the final chunk.
     *
     * When still reading the file, use `TAG_MESSAGE`, the most common tag, that doesn't add any
     * information about the nature of the message.
     */
    public function getTag(): int
    {
        return ($this->isEof())
            ? SODIUM_CRYPTO_SECRETSTREAM_XCHACHA20POLY1305_TAG_FINAL
            : SODIUM_CRYPTO_SECRETSTREAM_XCHACHA20POLY1305_TAG_MESSAGE;
    }
}
