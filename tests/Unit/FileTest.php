<?php

declare(strict_types=1);

namespace Steefdw\FileCrypt\Tests\Unit;

use PHPUnit\Framework\TestCase;
use Steefdw\FileCrypt\Exceptions\InvalidFilePermissions;
use Steefdw\FileCrypt\Exceptions\InvalidKey;
use Steefdw\FileCrypt\FileCrypt;

final class FileTest extends TestCase
{
    private string $writePath;

    /**
     * This method is called before each test.
     */
    public function setUp(): void
    {
        $this->writePath = dirname(__DIR__) . '/Artifacts/tmp/';
        if (!is_dir($this->writePath)) {
            mkdir($this->writePath, 0777, true);
        }
        if (!\extension_loaded('sodium')) {
            $this->markTestSkipped('The Libsodium extension is not enabled');
        }
    }

    /**
     * This method is called after each test.
     */
    protected function tearDown(): void
    {
        foreach ((array) glob($this->writePath . '*') as $file) {
            if ($file && is_file($file)) {
                unlink($file);
            }
        }
    }

    /**
     * When a test throws an exception, also clean up all artifacts.
     */
    public function __destruct()
    {
        $this->tearDown();
    }

    /** @test */
    public function it_should_generate_a_valid_key(): void
    {
        $key = FileCrypt::generateKey();

        $this->assertTrue(strlen($key) === SODIUM_CRYPTO_STREAM_KEYBYTES);
        $this->assertTrue(strlen(sodium_bin2hex($key)) === 64);
    }

    /** @test */
    public function it_should_validate_the_key_when_encrypting(): void
    {
        $someFile = $this->createRandomPath();
        file_put_contents($someFile, '::content::' . __FUNCTION__);

        $this->expectException(InvalidKey::class);
        FileCrypt::encrypt($someFile, $someFile, '::invalid::');
    }

    /** @test */
    public function it_should_validate_the_key_when_decrypting(): void
    {
        $someFile = $this->createRandomPath();

        file_put_contents($someFile, '::content::' . __FUNCTION__);

        $this->expectException(InvalidKey::class);
        FileCrypt::decrypt($someFile, $someFile, '::invalid::');
    }

    /** @test */
    public function it_should_validate_read_path_when_encrypting(): void
    {
        $this->expectException(InvalidFilePermissions::class);
        $this->expectExceptionMessage('Could not open file path for reading');
        $key = FileCrypt::generateKey();

        FileCrypt::encrypt('', '', $key);
    }

    /** @test */
    public function it_should_validate_read_path_when_decrypting(): void
    {
        $this->expectException(InvalidFilePermissions::class);
        $this->expectExceptionMessage('Could not open file path for reading');
        $key = FileCrypt::generateKey();

        FileCrypt::decrypt('', '', $key);
    }

    /** @test */
    public function it_should_validate_write_path_when_encrypting(): void
    {
        $this->expectException(InvalidFilePermissions::class);
        $this->expectExceptionMessage('Could not open file path for writing');
        $key = FileCrypt::generateKey();
        $originalFile = $this->writePath . 'test.txt';
        file_put_contents($originalFile, '::content::' . __FUNCTION__);

        FileCrypt::encrypt($originalFile, '', $key);
    }

    /** @test */
    public function it_should_create_write_path_when_encrypting(): void
    {
        $key = FileCrypt::generateKey();
        $originalFile = $this->createRandomPath();
        $encryptedFile = $this->createRandomPath('enc');
        file_put_contents($originalFile, '::content::' . __FUNCTION__);

        FileCrypt::encrypt($originalFile, $encryptedFile, $key);
        $this->assertFileExists($encryptedFile);
    }

    /** @test */
    public function it_should_validate_write_path_when_decrypting(): void
    {
        $this->expectException(InvalidFilePermissions::class);
        $this->expectExceptionMessage('Could not open file path for writing');
        $key = FileCrypt::generateKey();
        $originalFile = $this->createRandomPath();
        file_put_contents($originalFile, '::content::' . __FUNCTION__);

        FileCrypt::decrypt($originalFile, '', $key);
    }

    /** @test */
    public function it_should_encrypt_and_decrypt_a_file(): void
    {
        $originalFile = $this->createRandomPath();
        $encryptedFile = $this->createRandomPath('enc');
        $decryptedFile = $this->createRandomPath('dec');
        $plainText = '::content::' . __FUNCTION__;

        file_put_contents($originalFile, $plainText);

        $key = FileCrypt::generateKey();
        FileCrypt::encrypt($originalFile, $encryptedFile, $key);
        FileCrypt::decrypt($encryptedFile, $decryptedFile, $key);

        $encryptedText = file_get_contents($encryptedFile, true);
        $decryptedText = file_get_contents($decryptedFile, true);

        $this->assertNotEquals($plainText, $encryptedText);
        $this->assertSame($plainText, $decryptedText);
    }

    /**
     * Create a random file path in the test artifacts' folder.
     * Tries to be random, without being too strict.
     *
     * @see https://xkcd.com/221/
     */
    protected function createRandomPath(string $extension = 'txt'): string
    {
        try {
            $random = random_bytes(12);
            $randomFilename = str_replace('/', 'x', base64_encode($random));
        } catch (\Exception $exception) { // php7.4 needs setting $exception
            $randomFilename = 'not-so-random-name';
        }

        return $this->writePath . time() . $randomFilename . '.' . $extension;
    }
}
