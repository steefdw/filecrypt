# FileCrypt: Easy File Encryption and Decryption in PHP

**FileCrypt** is a simple yet powerful PHP package designed to help developers easily encrypt and decrypt files. This library utilizes the Libsodium cryptography library, ensuring strong encryption standards. 

Cypto is hard, but encrypting / decrypting sensitive files shouldn't be.

## Installation

```bash
composer require steefdw/filecrypt
```

## Usage

How to use FileCrypt:
* First, generate a key and store it somewhere safe
* After that you can easily encrypt of decrypt files

**Tip:** if you store the files, and you persist the S3/file paths in a database,
you could also generate a key for each file, and save that key in your database alongside the S3/file path. 

### Generating a Secret Key

You can generate a new secret key using the FileCrypt::generateKey() method:

```php
$secretKey = FileCrypt::generateKey();
```

### Encrypting files

```php
FileCrypt::encrypt(
    'path/to/input-file.txt', 
    'path/to/output-file.encrypted', 
    $secretKey
);
```

### Decrypting files

```php
FileCrypt::decrypt(
    'path/to/input-file.encrypted', 
    'path/to/output-file.decrypted', 
    $secretKey
);
```

### Storing the key

`FileCrypt::generateKey()` generates a binary sting, so expect something like: `���m���yS'������H�&�@�'4%�\F�3`.

And not a key like: `abcdefg1234567890hijkjlmnopk`.

So, if you want to store the key as text somewhere --like putting it in an `.env` variable-- save it as:

```php
$secretKey = FileCrypt::generateKey();
$base64EncodedStoredSecret = base64_encode($secretKey);

// Don't forget to decode it before using:
$secretKey = base64_decode($base64EncodedStoredSecret);
```

You can find a simple working example in [example.php](./example.php).

## Testing

```bash
composer test
```

Or:

```bash
make test
```

## Contributing

* make sure the code validation succeeds. You can run it with `make validate`. 
* make sure all tests succeed. You can run them with `make test`. 
* make sure all code is tested. 

## License

**FileCrypt** is released under the MIT license. Please see [Licence File](./LICENCE.md) for more information.
