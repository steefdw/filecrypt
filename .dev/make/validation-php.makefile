## Run all PHP validations
validate: syntax stan cs cs-fixer md

## Validate the PHP syntax
syntax:
	@echo '🔍 syntax check' && \
	find src tests -name "*.php" -print0 | xargs -0 -n1 -P8 php -l

## Validate the PHP code-style (phpcs)
cs:
	@echo '🔍 phpcs' && \
	./vendor/bin/phpcs --standard=.dev/config/phpcs.xml --colors -ps

## Validate the PHP code-style (php-cs-fixer)
cs-fixer:
	@echo '🔍 php-cs-fixer' && \
	./vendor/bin/php-cs-fixer fix src tests \
	--dry-run --diff --allow-risky=yes --config=.dev/config/php-cs-fixer.php

## Validate the PHP code-style (phpmd)
md:
	@echo '🔍 phpmd' && \
	./vendor/bin/phpmd src ansi .dev/config/phpmd.xml

## Validate the PHP code statically (phpstan)
stan:
	@echo '🔍 phpstan' && \
	./vendor/bin/phpstan analyse --memory-limit=2G --configuration=.dev/config/phpstan.neon

## Fix the PHP code-style (phpcbf)
fix:
	@echo '🔍 phpcbf' && \
	./vendor/bin/phpcbf src tests --colors
