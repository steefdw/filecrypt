<?php

declare(strict_types=1);

require __DIR__ . '/vendor/autoload.php';

use Steefdw\FileCrypt\FileCrypt;

const EXAMPLE_ENV = 'example.README.env';

$originalFilePath = './README.md';
$encryptedFilePath = './example.README.encrypted.txt';
$decryptedFilePath = './example.README.decrypted.md';

$secretKey = config('filecrypt.secret');
if (!$secretKey) {
    $secretKey = FileCrypt::generateKey();
    writeKey($secretKey);
}

FileCrypt::encrypt(
    $originalFilePath,
    $encryptedFilePath,
    $secretKey
);

FileCrypt::decrypt(
    $encryptedFilePath,
    $decryptedFilePath,
    $secretKey
);

/**
 * Normally you would manually put the value of $environmentVariable in something like an `.env` file.
 */
function writeKey(string $key): void
{
    $environmentVariable = PHP_EOL . 'FILECRYPT_SECRET=' . base64_encode($key);

    file_put_contents(EXAMPLE_ENV, $environmentVariable, FILE_APPEND);
}

/**
 * Fake Laravel `config()` helper function.
 *
 * Normally you probably have some helper method from your framework that gets this secret for you.
 * So for this example, let's emulate the Laravel `config()` helper method.
 *
 * @noinspection PhpUnusedParameterInspection
 */
function config(string $configKey): ?string
{
    if (!file_exists(EXAMPLE_ENV)) {
        return null;
    }

    $secretKey = fakeFilecryptConfigFile()['secret'];
    if (!$secretKey || !FileCrypt::isValidKey($secretKey)) {
        return null;
    }

    return $secretKey;
}

/**
 * When using Laravel, this would be the contents of your `config/filecrypt.php` file.
 *
 * @return array{secret: string}
 */
function fakeFilecryptConfigFile(): array
{
    return [
        'secret' => (string) base64_decode(env('FILECRYPT_SECRET')),
    ];
}

/**
 * Fake Laravel `env()` helper function.
 *
 * @noinspection PhpUnusedParameterInspection
 */
function env(string $envKey): string
{
    return (string) file_get_contents(EXAMPLE_ENV, false, null, -44, 44);
}
